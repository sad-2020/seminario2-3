const semver = require('semver');
const registerBD = [];
class ServiceRegistry {
    constructor(log) {
        this.log = log;
        this.services = {};
        this.timeout = 10;
    }

    get(name, version) {

        for (let i in registerBD){

            var value = registerBD[i];

            let objetoN = value.nombre;
            let objetoV = value.version;

            if (objetoN == name && objetoV == version) {
                return value;
            }
        }

    }

    register(name, version, ip, port) {
        //this.cleanup();
        const key = name + version + ip + port;

        this.services = {nombre:name, version:version, ip:ip, port:port};

           registerBD.push(this.services);

            console.log(`Added services ${name}, version ${version} at ${ip}:${port}`);
            console.log(registerBD);
            return registerBD;


    }

    unregister(name, version, ip, port) {
        const key = name + version + ip + port;
        delete this.services[key];
        return key;
    }

    cleanup() {
        const now = Math.floor(new Date() / 1000);
        Object.keys(this.services).forEach((key) => {
            if (this.services[key].timestamp + this.timeout < now) {
                delete this.services[key];
                console.log(`Removed service ${key}`);
            }
        });
    }
}

module.exports = ServiceRegistry;
