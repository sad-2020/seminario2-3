const express = require('express');
const ServiceRegistry = require('./lib/ServiceRegistry');
const service = express();
const serviceRegistry = new ServiceRegistry();

    service.put('/register/:servicename/:serviceversion/:serviceport', function(req, res) {

        const { servicename, serviceversion, serviceport } = req.params;
        const serviceip = req.connection.remoteAddress.includes('::')  ? `[${req.connection.remoteAddress}]` : req.connection.remoteAddress;
        const serviceKey = serviceRegistry.register(servicename, serviceversion, serviceip, serviceport);
        return res.json({ result: serviceKey });
    });

    service.get('/find/:servicename/:serviceversion', function(req, res)  {

        const { servicename, serviceversion } = req.params;
            let registro = serviceRegistry.get(servicename,serviceversion);
            if (!registro) return res.status(404).json({ result: 'Service not found' });

            console.log(registro);
            res.json(registro);
     });
    service.use((error, req, res, next) => {
        res.status(error.status || 500);
        return res.json({
            error: {
                message: error.message,
            },
        });
    });

    service.listen(5000);

module.exports = service;